use std::{net::SocketAddr, time::Duration};

use anyhow::{bail, Result};
use clap::Parser;
use gstreamer::{prelude::*, State};
use log::{debug, info, warn};
use tokio::sync::oneshot;

struct PipelineWrapper {
    pipeline: gstreamer::Pipeline,
    error_rx: tokio::sync::watch::Receiver<Result<()>>,
}

impl PipelineWrapper {
    async fn new(addr: SocketAddr) -> Result<Self> {
        info!("Starting pipeline for {}", addr);
        let pipeline = gstreamer::parse_launch(
            "pipewiresrc ! video/x-raw, format=YUY2, framerate=20/1 ! videoconvert
             ! video/x-raw, height=480, width=640, framerate=20/1 ! queue
             ! vp8enc keyframe-max-dist=50 deadline=1 ! rtpvp8pay ! udpsink name=sink",
        )?;

        let pipeline = pipeline
            .downcast::<gstreamer::Pipeline>()
            .map_err(|_| anyhow::anyhow!("not a pipeline"))?;

        let bus = pipeline.bus().ok_or_else(|| anyhow::anyhow!("No bus?"))?;

        let sink = pipeline
            .by_name("sink")
            .ok_or_else(|| anyhow::anyhow!("sink not found"))?;
        let () = sink.emit_by_name("clear", &[]);
        let () = sink.emit_by_name("add", &[&addr.ip().to_string(), &(addr.port() as i32)]);

        let (error_tx, error_rx) = tokio::sync::watch::channel(Ok(()));

        let pipeline_clone = pipeline.clone();
        bus.set_sync_handler(move |_, msg| {
            if let Some(src) = msg.src() {
                if src.name() == pipeline_clone.name() {
                    if let gstreamer::MessageView::Error(error) = msg.view() {
                        let e = anyhow::anyhow!("Pipeline error: {:?}", error);
                        warn!("{}", e);
                        let _ = error_tx.send_replace(Err(e));
                    }
                }
            }
            gstreamer::BusSyncReply::Drop
        });

        let pipeline = PipelineWrapper { pipeline, error_rx };
        pipeline.set_state(State::Playing).await?;
        Ok(pipeline)
    }

    async fn set_state(&self, state: State) -> Result<()> {
        let (state_tx, state_rx) = oneshot::channel();

        self.pipeline.call_async(move |pipeline| {
            let s = pipeline.set_state(state);
            let _ = state_tx.send(s);
        });

        state_rx.await??;
        Ok(())
    }

    async fn stop(self) -> Result<()> {
        self.set_state(State::Null).await?;
        Ok(())
    }

    async fn became_unhappy(&mut self) -> Result<()> {
        loop {
            {
                let e = self.error_rx.borrow();
                if let Err(err) = &*e {
                    bail!("Unhappy pipeline: {}", err);
                }
            }
            self.error_rx.changed().await?;
        }
    }
}

#[derive(clap::Parser)]
struct Opts {
    #[arg(short, long, default_value = "8888")]
    port: u16,
    hostname: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();
    let opt = Opts::parse();
    gstreamer::init()?;

    let mut pipeline = None;
    loop {
        let addrs = tokio::net::lookup_host((opt.hostname.as_str(), opt.port)).await;
        match (addrs, &mut pipeline) {
            (Ok(mut a), None) => {
                info!("Starting stream as {} is now resolvable", opt.hostname);
                pipeline = if let Some(a) = a.next() {
                    Some(PipelineWrapper::new(a).await?)
                } else {
                    None
                };
            }
            (Err(_), Some(_)) => {
                info!(
                    "Stopping stream as {} is no longer resolvable",
                    opt.hostname
                );
                pipeline.take().unwrap().stop().await?;
            }
            (Ok(_), Some(p)) => {
                debug!("Everything happy... for now");
                if let Ok(Err(e)) =
                    tokio::time::timeout(Duration::from_secs(2), p.became_unhappy()).await
                {
                    warn!("Pipeline became unhappy: {}", e);
                    // Rather then retrying give up and rely on e.g. systemd to restart us just
                    // in case there is some process state that causes issues (e.g. gstreamer
                    // leaking file descriptors
                    return Err(e);
                }
            }
            (Err(e), None) => {
                debug!("Waiting for host, not resolvable: {}", e);
                tokio::time::sleep(Duration::from_secs(2)).await;
            }
        };
    }
}
